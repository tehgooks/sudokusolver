#include "Sudoku.h"

int nSolved = nMaxNumber * nMaxNumber;
int nGiven = 0;

int main()
{
	int ** puzzle;
	Cell ***sudoku;
	Block ***blocks;
	int nIteration = 0;
	int nInitialSolved;
	puzzle = createPuzzle();
	printPuzzle(puzzle);

	blocks = createBlock();

	sudoku = createSudoku(puzzle, blocks);
	if(!isValidSudoku(sudoku, blocks))
	{
		printf("Invalid Puzzle");
	}
	else {
		nGiven = nSolved;
		do
		{
			nInitialSolved = nSolved;
			solvePuzzle(sudoku, blocks);
			nIteration++;
		}while(nInitialSolved != nSolved);
		printSudoku(sudoku);

		if(!isValidSudoku(sudoku, blocks))
		{
			printf("inValid Solution");
		}
		else
		{
			if(nSolved == 0)
			{
				printf("Solved the puzzle after %d iterations",nIteration);
			}
			else
			{
				trialAndErrorSolve(sudoku, blocks);
				printSudoku(sudoku);
				printf("Gave up on the puzzle puzzle after %d iterations\n",nIteration);
				printf("Given %d squares, solved %d squares, for a total of %d solved squares\n",(nMaxNumber * nMaxNumber) - nGiven, nGiven - nSolved,(nMaxNumber * nMaxNumber) - nSolved);
				printf("Solved the puzzle using trial and error");
			}
		}
	}
	return 0;
}
