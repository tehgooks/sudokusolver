/*
 * sudoku.h
 *
 *  Created on: Oct 20, 2015
 *      Author: tho
 */
#include <stdio.h>
#include <stdlib.h>

#ifndef HEADER_sudoku_H_
#define HEADER_sudoku_H_

typedef int bool;

#define true 1
#define false 0

#define printHorizontalGrid for (x = 0; x <= nMaxNumber + ((1+ (2*nBlockSize)) * nBlockSize); x++) printf("-"); // Print out a horizontal line

#define nBlockSize 3
#define nMaxNumber nBlockSize * nBlockSize
#define unSolved 0

//#define CSV

#ifdef CSV
	#define csvPrint printf
#else
	#define csvPrint(...)
#endif

typedef struct
{
	// The actual value of the square
	int number;

	// Possible values of the square
	int notes;

	// The amount of numbers possible by square
	int possible;

	// Row location of the cell
	int nRow;

	// Column location of the cell
	int nColumn;
} Cell;

typedef struct
{
	int nRow;
	int nColumn;
} Block;

void 	solvePuzzle							(Cell*** sudoku, Block*** blocks);

Block***createBlock							();
int**	createPuzzle						();
Cell***	createSudoku						(int **puzzle, Block*** blocks);
void	printSudoku							(Cell*** sudoku);
void	printPuzzle							(int **puzzle);
void	initialNotes						(Cell*** sudoku, Block*** blocks);

// Validation for the initial puzzle
bool	isValidSudoku						(Cell*** sudoku, Block*** blocks);
bool	isRowValid							(Cell*** sudoku);
bool	isColumnValid						(Cell*** sudoku);
bool	isBlocksValid						(Cell*** sudoku, Block*** blocks);

void 	removeNotes							(Cell* cell, int nNote, Cell*** sudoku, Block*** blocks);
void	removeNotesRow						(Cell *cell, int nNote, Cell*** sudoku);
void	removeNotesColumn					(Cell *cell, int nNote, Cell*** sudoku);
void	removeNotesBlocks					(Cell *cell, int nNote, Cell*** sudoku, Block*** blocks);

void	fillSolved							(Cell*** sudoku,Block*** blocks);
void	printNotes							(Cell*** sudoku);

void	ifOnlyPossible						(Cell*** sudoku, Block*** blocks);
void	ifOnlyPossibleRow					(Cell*** sudoku, Block*** blocks);
void	ifOnlyPossibleColumn				(Cell*** sudoku, Block*** blocks);
void	ifOnlyPossibleBlocks				(Cell*** sudoku, Block*** blocks);

void	removePossibleRowsColumnsAndBlocks	(Cell*** sudoku, Block*** blocks);

void	nakedSubsets						(Cell*** sudoku, Block*** blocks);
void 	nakedSubSetsRow						(int subset, Cell*** sudoku);
void	nakedSubSetsColumn					(int subset, Cell*** sudoku);
void	nakedSubsetsBlocks					(int subset, Cell*** sudoku, Block*** blocks);

bool	trialAndErrorSolve					(Cell*** sudoku, Block*** blocks);
Cell* findNextEmptyCell(Cell*** sudoku);
/*
void	subSetRowsAndColumns				(Cell*** sudoku);
void 	subSetBlocks						(Cell*** sudoku, Block*** blocks);
int*	allPossibleCombinations				(int nNotes, int nPossible);
*/
#endif /* HEADER_sudoku_H_ */
