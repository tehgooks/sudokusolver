#include "Sudoku.h"

extern int nSolved;

//#define easy		// solved   - 13 Iterations | solved   - 6 iterations 	| solved - 4 iterations		| solved - 3 iterations		| solved - 3 iterations
//#define normal	// solved 3 - 7 Iterations	| solved 3 - 3 Iterations	| solved 13 - 4 iterations	| solved - 6 iterations		| solved - 6 iterations
//#define hard 		// solved 1 - 2 Iterations	| solved 1 - 2 Iterations	| solved 11 - 4 iterations	| solved 11 - 4 iterations	| Solved - 7 iterations
#define extreme	// solved 0 - 1 Iterations	| solved 0 - 1 Iterations	| solved 5 - 2 iterations	| solved 5 - 2 iterations	| Solved 5 - 2 iterations

//====================================================================
//	Name: 	solvePuzzle
//	Desc:	Will perform all the operations to remove "invalid" notes.
//	Args:
//			[I/O]	sudoku:	A pointer to a two dimensional array of
//							Cell.
//			True:	A new cell have been solved.
//			False:	There have been no new solved cells.
//====================================================================
void solvePuzzle(Cell*** sudoku, Block*** blocks)
{
	ifOnlyPossible(sudoku, blocks);
	removePossibleRowsColumnsAndBlocks(sudoku, blocks);
	nakedSubsets(sudoku, blocks);
	fillSolved(sudoku, blocks);
}

//====================================================================
//	Name: 	createPuzzle
//	Desc:	Create a multidimensional array of integers that will
//			represent the original puzzle.
//	Args:	N/A
//	Return:	Two dimensional array of integers.
//====================================================================
int ** createPuzzle()
{
	int **puzzle;
	int nRow, nColumn;

#if defined(easy)
	int array[nMaxNumber][nMaxNumber] =	{
											{	0, 3, 0,	6, 4, 7,	0, 8, 0 	},
											{	7, 0, 9,	0, 0, 0,	2, 0, 6 	},
											{	0, 1, 0,	9, 0, 3,	0, 4, 0 	},

											{	3, 0, 1,	0, 7, 0,	8, 0, 4 	},
											{	8, 0, 0,	3, 0, 4,	0, 0, 2 	},
											{	4, 0, 2,	0, 5, 0,	6, 0, 3 	},

											{	0, 8, 0,	5, 0, 1,	0, 2, 0 	},
											{	1, 0, 3,	0, 0, 0,	4, 0, 9 	},
											{	0, 2, 0,	4, 3, 9,	0, 6, 0 	}
										};
#endif

#if defined(normal)
	int array[nMaxNumber][nMaxNumber] =	{
											{	0, 0, 0,	1, 8, 4,	0, 0, 0 	},
											{	0, 4, 0,	0, 0, 0,	0, 3, 0 	},
											{	7, 0, 9,	6, 0, 2,	1, 0, 5 	},

											{	0, 9, 7,	0, 0, 0,	4, 2, 0 	},
											{	6, 0, 0,	0, 0, 0,	0, 0, 7 	},
											{	0, 2, 8,	0, 0, 0,	3, 9, 0 	},

											{	9, 0, 2,	4, 0, 8,	7, 0, 3 	},
											{	0, 5, 0,	0, 0, 0,	0, 8, 0 	},
											{	0, 0, 0,	2, 9, 1,	0, 0, 0 	}
										};
#endif

#if defined(hard)
	int array[nMaxNumber][nMaxNumber] =	{
											{	0, 0, 1,	0, 9, 3,	0, 0, 6, 	},
											{	0, 2, 3,	6, 1, 0,	0, 7, 0, 	},
											{	0, 0, 0,	0, 0, 0,	0, 2, 0, 	},

											{	9, 0, 0,	0, 0, 0,	0, 0, 0, 	},
											{	0, 0, 5,	3, 6, 1,	7, 0, 0, 	},
											{	0, 0, 0,	0, 0, 0,	0, 0, 4, 	},

											{	0, 8, 0,	0, 0, 0,	0, 0, 0, 	},
											{	0, 9, 0,	0, 8, 6,	3, 5, 0, 	},
											{	1, 0, 0,	4, 5, 0,	8, 0, 0, 	}
										};
#endif

#if defined(extreme)
	int array[nMaxNumber][nMaxNumber] =	{
											{	3, 0, 0,	0, 0, 0,	0, 0, 9 	},
											{	0, 7, 0,	0, 4, 0,	0, 3, 0 	},
											{	0, 0, 6,	1, 0, 3,	5, 0, 0 	},

											{	0, 0, 7,	0, 3, 0,	8, 0, 0 	},
											{	0, 8, 0,	2, 0, 4,	0, 1, 0 	},
											{	0, 0, 5,	0, 7, 0,	6, 0, 0 	},

											{	0, 0, 2,	3, 0, 7,	4, 0, 0 	},
											{	0, 1, 0,	0, 6, 0,	0, 2, 0 	},
											{	8, 0, 0,	0, 0, 0,	0, 0, 7 	}
										};
#endif

	// Allocates memory for a integer pointer of size int * nMaxNumber.
	puzzle = (int**)(malloc(sizeof(int*)*nMaxNumber));

	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		// Allocates memory for a integer pointer of size int * naxNumber
		// inside of int*. Essentially creating a 2 Dimensional Array
		puzzle[nRow] = (int*)malloc(sizeof(int)*nMaxNumber);
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			// Initialize set the **integer and set the value to the mirror value in array
			puzzle[nRow][nColumn] = array[nRow][nColumn];
		}
	}

	return puzzle;
}


//====================================================================
//	Name: 	createSudoku
//	Desc: 	Create the sudoku puzzle, with empty cells have all
//			the notes filled with valid values.
//	Args:
//			[IN]	puzzle:	multidimensional array of the original
//							puzzle.
//			[I/O]	blocks:	A pointer to a multidimensional array of Block
//	Return:	A pointer to a two-dimensional array of cells.
//====================================================================
Cell*** createSudoku(int **puzzle, Block*** blocks)
{
	int nRow, nColumn;
	Cell ***sudoku;
	sudoku = (Cell***)malloc(sizeof(Cell**)*nMaxNumber);

	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		sudoku[nRow] = (Cell**)malloc(sizeof(Cell*)*nMaxNumber);

		for (nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			sudoku[nRow][nColumn] = (Cell*)malloc(sizeof(Cell)*nMaxNumber);

			// If the number is known, then make notes set everything else to zero
			if (puzzle[nRow][nColumn] != 0)
			{
				sudoku[nRow][nColumn]->number 	= puzzle[nRow][nColumn];
				sudoku[nRow][nColumn]->possible = 0;
				nSolved--;
			}
			else
			{
				sudoku[nRow][nColumn]->number 	= unSolved;
				sudoku[nRow][nColumn]->possible = nMaxNumber;
			}

			sudoku[nRow][nColumn]->notes 	= 0;
			sudoku[nRow][nColumn]->nRow 	= nRow;
			sudoku[nRow][nColumn]->nColumn 	= nColumn;
		}
	}

	initialNotes(sudoku, blocks);
	return sudoku;
}

//====================================================================
//	Name: 	createBlock
//	Desc: 	Creates a two dimensional array of blocks that holds the
// 			coordinates of each block, all the blocks in Row 0 contains
//			the coordinates of each block in Block 0.
//	Args:	N/A
//	Return:	A pointer to a two dimensional array of Block.
//====================================================================
Block *** createBlock()
{
	Block*** blocks;
	int nRow, nColumn;
	blocks = (Block***)malloc(sizeof(Block**)*nMaxNumber);

	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		blocks[nRow] = (Block**)malloc(sizeof(Block*)*nMaxNumber);
		for (nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			blocks[nRow][nColumn] = (Block*)malloc(sizeof(Block)*nMaxNumber);
		}
	}

	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for (nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			// Magical algorithm that Tho did... don't know if this will work outside of a 9x9 puzzle...
			blocks[nRow][nColumn]->nRow 		= (nColumn / nBlockSize) + (nRow/nBlockSize) + ((nBlockSize - 1) * (nRow/nBlockSize));
			blocks[nRow][nColumn]->nColumn 	 	= (nColumn % nBlockSize) + ((nRow % nBlockSize) * nBlockSize);
		}
	}
	return blocks;
}

//====================================================================
//	Name: 	printPuzzle
//	Desc: 	Will print out the puzzle in a pretty format.
//	Args:
//			[IN]	puzzle:	Multidimensional array of the original
//							puzzle.
//	Return:	N/A
//====================================================================
void printPuzzle(int **puzzle)
{
	int nRow, nColumn;

#ifdef CSV
	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for (nColumn = 0; nColumn< nMaxNumber; nColumn++)
		{
			csvPrint("%d",puzzle[nRow][nColumn]);
			if (!((nColumn == (nMaxNumber -1)) && (nRow == (nMaxNumber -1))))
			{
				csvPrint(",");
			}
		}
	}
#else

	int x;
	printHorizontalGrid;
	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		printf("\n|");
		for (nColumn = 0; nColumn< nMaxNumber; nColumn++)
		{
			// Print out a empty cell as blank
			if (puzzle[nRow][nColumn] == 0){
				// Prints out a black space instead of 0 on unsolved cells
				printf("   ");

			}
			else
			{
				printf(" %d ",puzzle[nRow][nColumn]);
			}
			if (((nColumn+1) % nBlockSize) == 0)
			{
				printf("|");
			}
		}
		if (((nRow+1) % nBlockSize) == 0)
		{
			printf("\n");
			printHorizontalGrid;
		}
	}
	printf("\n");
#endif
}

//====================================================================
//	Name: 	isRowValid
//	Desc: 	Checks each row for repeated numbers
//	Args:
//			[I/O]	sudoku:	A pointer to a two dimensional array of
//							Cell.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	[N/A]
//====================================================================
void initialNotes(Cell*** sudoku, Block*** blocks)
{
	int nRow, nColumn, x;
	Cell* currentCell;
	csvPrint("Filling in initial notes of the puzzle.\n");
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			currentCell = sudoku[nRow][nColumn];
			if(currentCell->number != unSolved) continue;
			for(x = 0; x < nMaxNumber; x++)
			{
				currentCell->number = x+1;
				if(isValidSudoku(sudoku,blocks))
				{
					currentCell->notes |= (1 << x);
					csvPrint("%d,%d,|%d\n",currentCell->nRow,currentCell->nColumn,currentCell->notes);
				}
				else
				{
					currentCell->possible--;
				}
				currentCell->number = unSolved;
			}
		}
	}
	csvPrint("Finished filling in initial notes\n");
}

//====================================================================
//	Name: 	isValidSudoku
//	Desc: 	Check that the puzzle is valid
//	Args:
//			[I/O]	sudoku:	A pointer to a two dimensional array of
//							Cell.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	Bool:
//				True: The puzzle is valid
//				False: The puzzle is not valid

//====================================================================
bool isValidSudoku(Cell*** sudoku, Block*** blocks)
{
	return (isRowValid(sudoku) & isColumnValid(sudoku) & isBlocksValid(sudoku, blocks));
}

//====================================================================
//	Name: 	isRowValid
//	Desc: 	Checks each row for repeated numbers
//	Args:
//			[I/O]	sudoku:	A pointer to a two dimensional array of
//							Cell.
//	Return:	Bool:
//				True: There are no repeated numbers in each row.
//				False: There is a repeated number in a row.
//====================================================================
bool isRowValid(Cell*** sudoku)
{
	bool isValid = true;
	int nRow, nColumn;
	int x;

	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			if(sudoku[nRow][nColumn]->number == unSolved) continue;
			for(x = 0; x < nMaxNumber; x++)
			{
				if(x == nColumn) continue;
				if(sudoku[nRow][nColumn]->number == unSolved) continue;
				if(sudoku[nRow][x]->number == sudoku[nRow][nColumn]->number)
				{
					isValid = false;
					break;
				}
			}
			if(isValid == false) break;
		}
		if(isValid == false) break;
	}
	return isValid;
}

//====================================================================
//	Name: 	isRowValid
//	Desc: 	Checks each column for repeated numbers
//	Args:
//			[I/O]	sudoku:	A pointer to a two dimensional array of
//							Cell.
//	Return:	Bool:
//				True: There are no repeated numbers in each column.
//				False: There is a repeated number in a column.
//====================================================================
bool isColumnValid(Cell*** sudoku)
{
	{
		bool isValid = true;
		int nRow, nColumn;
		int x;

		for(nRow = 0; nRow < nMaxNumber; nRow++)
		{
			for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
			{
				if(sudoku[nRow][nColumn]->number == unSolved) continue;
				for(x = 0; x < nMaxNumber; x++)
				{
					if(x == nRow) continue;
					if(sudoku[x][nColumn]->number == unSolved) continue;
					if(sudoku[x][nColumn]->number == sudoku[nRow][nColumn]->number)
					{
						isValid = false;
						break;
					}
				}
				if(isValid == false) break;
			}
			if(isValid == false) break;
		}
		return isValid;
	}
}

//====================================================================
//	Name: 	isBlocksValid
//	Desc: 	Checks each row for repeated numbers
//	Args:
//			[I/O]	sudoku:	A pointer to a two dimensional array of
//							Cell.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	Bool:
//				True: There are no repeated numbers in each block.
//				False: There is a repeated number in a block.
//====================================================================
bool isBlocksValid(Cell*** sudoku, Block*** blocks)
{
	bool isValid = true;
	int nRow, nColumn;
	int x;
	Block *currentBlock, *tempBlock;
	Cell *currentCell, *tempCell;
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			currentBlock = blocks[nRow][nColumn];
			currentCell = sudoku[currentBlock->nRow][currentBlock->nColumn];

			if(currentCell->number == unSolved) continue;

			for(x = 0; x < nMaxNumber; x++)
			{
				if(x == nColumn) continue;

				tempBlock = blocks[nRow][x];
				tempCell = sudoku[tempBlock->nRow][tempBlock->nColumn];

				if(tempCell->number == unSolved) continue;
				if(tempCell->number == currentCell->number)
				{
					isValid = false;
					break;
				}
			}
			if(isValid == false) break;
		}
		if(isValid == false) break;
	}
	return isValid;
}

//====================================================================
//	Name: 	printSudoku
//	Desc: 	Will print out the sudoku in a pretty format.
//	Args:
//			[I/O]	sudoku:	A pointer to an two dimensional array of
//							cells.
//	Return:	N/A
//====================================================================
void printSudoku(Cell*** sudoku)
{
	int nRow, nColumn;
#ifdef CSV
	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for (nColumn = 0; nColumn< nMaxNumber; nColumn++)
		{
			if(sudoku[nRow][nColumn]->number == unSolved)
			{
				csvPrint("|%d",sudoku[nRow][nColumn]->notes);
			}
			else
			{
				csvPrint("%d",sudoku[nRow][nColumn]->number);
			}
			if (!((nColumn == (nMaxNumber -1)) && (nRow == (nMaxNumber -1))))
			{
				csvPrint(",");
			}
		}
	}

#else
	int x;
	printHorizontalGrid;

	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		printf("\n|");
		for (nColumn = 0; nColumn< nMaxNumber; nColumn++)
		{
			if (sudoku[nRow][nColumn]->number == 0){
				// Prints out a black space instead of 0 on unsolved cells
				printf("   ");
			}
			else
			{
				printf(" %d ",sudoku[nRow][nColumn]->number);
			}

			if (((nColumn+1) % nBlockSize) == 0)
			{
				printf("|");
			}
		}
		if (((nRow+1) % nBlockSize) == 0)
		{
			printf("\n");
			printHorizontalGrid;
		}
	}
#endif
	printf("\n");
}

//====================================================================
//	Name: 	printNotes
//	Desc: 	Print out the notes in csv format
//	Args:
//			[I/O]	sudoku:	A pointer to an two dimensional array of
//							cells.
//	Return:	N/A
//====================================================================
void printNotes(Cell*** sudoku)
{
	int nRow, nColumn;
#ifdef CSV
	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for (nColumn = 0; nColumn< nMaxNumber; nColumn++)
		{
			if (sudoku[nRow][nColumn]->number == 0){
				csvPrint("|%d",sudoku[nRow][nColumn]->notes);
			}
			else
			{
				csvPrint("%d",sudoku[nRow][nColumn]->number);
			}
			if (!((nColumn == (nMaxNumber -1)) && (nRow == (nMaxNumber -1))))
			{
				csvPrint(",");
			}
		}
	}
	csvPrint("\n");
#else
	int i;
	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for (nColumn = 0; nColumn< nMaxNumber; nColumn++)
		{
			if (sudoku[nRow][nColumn]->number == 0){
				printf("|");
				for(i = 0; i < nMaxNumber; i++)
				{
					if((sudoku[nRow][nColumn]->notes & (1 << i)) != 0)
					{
						printf("%d ",i+1);
					}
				}
			}
			else
			{
				printf("%d",sudoku[nRow][nColumn]->number);
			}

			printf("\n");
		}
		printf("\n");
	}
#endif
}

//====================================================================
//	Name: 	removeNotes
//	Desc: 	Remove all the nNote from all the cells  in the row, column,
//			and blocks, other then the cell that was passed in.
//	Args:
//			[IN]	cell: 	The cell that is not to be the note removed
//			[IN]	nNote:	The number to be remove from the notes
//			[I/O]	sudoku:	A pointer to an two dimensional array of
//							cells.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	N/A
//====================================================================
void removeNotes(Cell* cell, int nNote, Cell*** sudoku, Block*** blocks)
{
	csvPrint("Removing Invalid Notes\n");
	removeNotesRow						(cell, nNote, sudoku);
	removeNotesColumn					(cell, nNote, sudoku);
	removeNotesBlocks					(cell, nNote, sudoku, blocks);
}

//====================================================================
//	Name: 	removeNotesRow
//	Desc: 	Remove all the nNote from all the cells  in the row, other
//			then the cell that was passed in.
//	Args:
//			[IN]	cell: 	The cell that is not to be the note removed
//			[IN]	nNote:	The number to be remove from the notes
//			[I/O]	sudoku:	A pointer to an two dimensional array of
//							cells.
//	Return:	N/A
//====================================================================
void removeNotesRow(Cell *cell, int nNote, Cell*** sudoku)
{
	int nColumn;
	Cell* currentCell;
	csvPrint("Removing Invalid Notes in the Rows\n");
	for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
	{
		if(nColumn == cell->nColumn) continue;
		currentCell = sudoku[cell->nRow][nColumn];
		if(currentCell->number != unSolved) continue;

		if ((currentCell->notes & (1 << nNote)) != 0)
		{
			currentCell->notes ^= (1 << nNote);
			currentCell->possible--;
			csvPrint("%d,%d,|%d\n",currentCell->nRow,currentCell->nColumn,currentCell->notes);
		}
	}
}

//====================================================================
//	Name: 	removeNotesColumn
//	Desc: 	Remove all the nNote from all the cells  in the column, other
//			then the cell that was passed in.
//	Args:
//			[IN]	cell: 	The cell that is not to be the note removed
//			[IN]	nNote:	The number to be remove from the notes
//			[I/O]	sudoku:	A pointer to an two dimensional array of
//							cells.
//	Return:	N/A
//====================================================================
void removeNotesColumn(Cell *cell, int nNote, Cell*** sudoku)
{
	int nRow;
	Cell* currentCell;
	csvPrint("Removing Invalid Notes in the Columns\n");
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		if(nRow == cell->nRow) continue;
		currentCell = sudoku[nRow][cell->nColumn];
		if(currentCell->number != unSolved) continue;

		if ((currentCell->notes & (1 << nNote)) != 0)
		{
			currentCell->notes ^= (1 << nNote);
			currentCell->possible--;
			csvPrint("%d,%d,|%d\n",currentCell->nRow,currentCell->nColumn,currentCell->notes);
		}
	}
}

//====================================================================
//	Name: 	removeNotesBlocks
//	Desc: 	Remove all the nNote from all the cells  in the blocks, other
//			then the cell that was passed in.
//	Args:
//			[IN]	cell: 	The cell that is not to be the note removed
//			[IN]	nNote:	The number to be remove from the notes
//			[I/O]	sudoku:	A pointer to an two dimensional array of
//							cells.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	N/A
//====================================================================
void removeNotesBlocks(Cell *cell, int nNote, Cell*** sudoku, Block*** blocks)
{
	int nRow, nColumn;

	int currentBlock;
	Cell* currentCell;
	csvPrint("Removing Invalid Notes in the Blocks\n");
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			if((blocks[nRow][nColumn]->nRow == cell->nRow) && (blocks[nRow][nColumn]->nColumn == cell->nColumn))
			{
				currentBlock = nRow;
				break;
			}
		}
	}


	for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
	{

		currentCell = sudoku[blocks[currentBlock][nColumn]->nRow][blocks[currentBlock][nColumn]->nColumn];
		if((blocks[currentBlock][nColumn]->nColumn == cell->nColumn) && (blocks[currentBlock][nColumn]->nRow == cell->nRow)) continue;
		if(currentCell->number != unSolved) continue;
		if ((currentCell->notes & (1 << nNote)) != 0)
		{
			currentCell->notes ^= (1 << nNote);
			currentCell->possible--;
			csvPrint("%d,%d,|%d\n",currentCell->nRow,currentCell->nColumn,currentCell->notes);
		}
	}
}

//====================================================================
//	Name: 	fillCell
//	Desc:	Solve the cell with the solution, and set the rest of the valeus
//			according.
//	Args:
//			[IN]	cell: The cell to be filled in.
//			[IN]	solution: The solution to the cell
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	[N/A]
//====================================================================
void fillCell(Cell *cell, int solution, Cell*** sudoku, Block*** blocks)
{
	cell->notes = 0;
	cell->possible = 0;
	cell->number = solution;
	nSolved--;
	csvPrint("%d,%d,%d\n",cell->nRow,cell->nColumn,cell->number);
	removeNotes(cell,solution-1,sudoku,blocks);
}

//====================================================================
//	Name: 	fillSolved
//	Desc:	Iterate through all the cells, and if there is only one
//			possible solution to the cell then fill it in.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:
//			True:	A new cell have been solved.
//			False:	There have been no new solved cells.
//====================================================================
void fillSolved(Cell*** sudoku, Block*** blocks)
{
	int nRow, nColumn, x;
	Cell* currentCell;
	for (nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for (nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			if (sudoku[nRow][nColumn]->possible != 1) continue;

			for(x = 0; x < nMaxNumber; x++)
			{
				currentCell = sudoku[nRow][nColumn];

				if ((currentCell->notes & (1 << x)) != 0)
				{
					fillCell(currentCell, x+1, sudoku, blocks);
					break;
				}
			}
		}
	}
}

//====================================================================
//	Name: 	ifOnlyPossible
//	Desc:	Find rows, columns, or boxes where there is only one possible
//			value.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	[N/A]
//====================================================================
void ifOnlyPossible(Cell*** sudoku, Block*** blocks)
{
	csvPrint("If Only Possible\n");
	ifOnlyPossibleRow(sudoku,blocks);
	ifOnlyPossibleColumn(sudoku,blocks);
	ifOnlyPossibleBlocks(sudoku,blocks);
}

//====================================================================
//	Name: 	ifOnlyPossibleRow
//	Desc:	Find rows where there is only one possible
//			value.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	[N/A]
//====================================================================
void ifOnlyPossibleRow(Cell*** sudoku, Block*** blocks)
{
	int nRow, nColumn, x, i;

	bool isFound;
	Cell* currentCell;
	Cell* tempCell;
	csvPrint("If Only Possible Row\n");
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			currentCell = sudoku[nRow][nColumn];
			if (currentCell->number != unSolved) continue;
			if (currentCell->possible == 1) continue;

			for(x = 0; x < nMaxNumber; x++)
			{
				if((currentCell->notes & (1 << x)) == 0) continue;
				isFound = false;
				for(i = 0; i < nMaxNumber; i++)
				{
					tempCell = sudoku[nRow][i];
					if (i == nColumn) continue;
					if (tempCell->number != unSolved) continue;

					if((tempCell->notes & (1 << x)) != 0)
					{
						isFound = true;
						break;
					}
				}
				if (isFound == false)
				{
					fillCell(currentCell, x+1, sudoku, blocks);
					nRow = 0;
					nColumn = 0;
					break;
				}
			}
		}
	}
}

//====================================================================
//	Name: 	ifOnlyPossibleColumn
//	Desc:	Find column where there is only one possible
//			value.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	[N/A]
//====================================================================
void ifOnlyPossibleColumn(Cell*** sudoku, Block*** blocks)
{
	int nRow, nColumn, x, i;

	bool isFound;
	Cell* currentCell;
	Cell* tempCell;
	csvPrint("If Only Possible Column\n");
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			currentCell = sudoku[nRow][nColumn];
			if (currentCell->number != unSolved) continue;
			if (currentCell->possible == 1) continue;

			for(x = 0; x < nMaxNumber; x++)
			{
				if((currentCell->notes & (1 << x)) == 0) continue;
				isFound = false;
				for(i = 0; i < nMaxNumber; i++)
				{
					tempCell = sudoku[i][nColumn];
					if (i == nRow) continue;
					if (tempCell->number != unSolved) continue;

					if((tempCell->notes & (1 << x)) != 0)
					{
						isFound = true;
						break;
					}
				}
				if (isFound == false)
				{
					fillCell(currentCell, x+1, sudoku, blocks);
					nRow = 0;
					nColumn = 0;
					break;
				}
			}
		}
	}
}

//====================================================================
//	Name: 	ifOnlyPossibleBlocks
//	Desc:	Find blocks where there is only one possible
//			value.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	[N/A]
//====================================================================
void ifOnlyPossibleBlocks(Cell*** sudoku, Block*** blocks)
{
	int nRow, nColumn, i, x;
	Block* currentBlock;
	bool bMultiple = false;
	Cell* currentCell;
	csvPrint("If Only Possible Blocks\n");
	for (nRow = 0; nRow < nMaxNumber; nRow++) {
		for (nColumn = 0; nColumn < nMaxNumber; nColumn++) {
			currentBlock = blocks[nRow][nColumn];
			currentCell = sudoku[currentBlock->nRow][currentBlock->nColumn];

			if (currentCell->number != unSolved)
				continue;
			if (currentCell->possible == 1)
				continue;

			// For each possible value in currentCell...
			for (x = 0; x < nMaxNumber; x++) {
				if ((currentCell->notes & (1 << x)) == 0)
					continue;
				bMultiple = false;

				// Loop through each cell in the block...
				for (i = 0; i < nMaxNumber; i++) {
					if (i == nColumn)
						continue;

					// If there is another cell in the row with the possible value
					// Then break...
					if ((sudoku[blocks[nRow][i]->nRow][blocks[nRow][i]->nColumn]->notes
							& (1 << x)) != 0) {
						bMultiple = true;
						break;
					}
				}
				// If another cell is not found then currentCell can only be that value
				if (bMultiple == false) {
					fillCell(currentCell, x+1, sudoku, blocks);
					nRow = 0;
					nColumn = 0;
					break;
				}
			}
		}
	}
}

//====================================================================
//	Name: 	removePossibleRowsColumnsAndBlocks
//	Desc:	Iterate through the rows and columns, if there is only one
// 			valid possible value in the box then fill it in.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	N/A
//====================================================================
void removePossibleRowsColumnsAndBlocks(Cell*** sudoku, Block*** blocks)
{
	int nRow, nColumn, i, x;
	bool bFound;
	Block* currentBlock;
	Block* tempBlock;
	Cell* currentCell;
	Cell* tempCell;

	csvPrint("Removing possible Rows Columns and Blocks\n");
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
			{
				// Get the current block
				currentBlock = blocks[nRow][nColumn];
				currentCell = sudoku[currentBlock->nRow][currentBlock->nColumn];

				if (currentCell->number != unSolved) continue;
				if (currentCell->possible == 1) continue;

				// Loop through each possible value for the cell.
				for(i = 0; i < nMaxNumber; i++)
				{
					if ((currentCell->notes & (1 << i)) == 0) continue;

					bFound = false;
					for(x = 0; x < nMaxNumber; x++)
					{
						tempBlock = blocks[nRow][x];
						tempCell = sudoku[tempBlock->nRow][tempBlock->nColumn];

						if ((nColumn / nBlockSize) == (x / nBlockSize)) continue;
						if (tempCell->number != unSolved) continue;
						if ((tempCell->notes & (1 << i)) == 0) continue;

						bFound = true;
						break;

					}
					if (bFound == false)
					{
						for(x = 0; x < nMaxNumber; x++)
						{
							if (sudoku[currentBlock->nRow][x]->possible == 1) continue;
							if (sudoku[currentBlock->nRow][x]->number != unSolved) continue;
							if ((x / nBlockSize) == (currentBlock->nColumn / nBlockSize)) continue;

							if((sudoku[currentBlock->nRow][x]->notes & (1 << i)) != 0)
							{
								sudoku[currentBlock->nRow][x]->possible--;
								sudoku[currentBlock->nRow][x]->notes ^= (1 << i);
								csvPrint("%d,%d,|%d\n",sudoku[currentBlock->nRow][x]->nRow,sudoku[currentBlock->nRow][x]->nColumn,sudoku[currentBlock->nRow][x]->notes);
							}
						}
					}

					bFound = false;
					for(x = 0; x < nMaxNumber; x++)
					{
						tempBlock = blocks[nRow][x];
						tempCell = sudoku[tempBlock->nRow][tempBlock->nColumn];

						if ((nColumn % nBlockSize) == (x % nBlockSize)) continue;
						if (tempCell->number != unSolved) continue;
						if ((tempCell->notes & (1 << i)) == 0) continue;

						bFound = true;
						break;

					}
					if (bFound == false)
					{
						for(x = 0; x < nMaxNumber; x++)
						{
							if (sudoku[x][currentBlock->nColumn]->possible == 1) continue;
							if (sudoku[x][currentBlock->nColumn]->number != unSolved) continue;
							if ((x / nBlockSize) == (currentBlock->nRow / nBlockSize)) continue;

							if((sudoku[x][currentBlock->nColumn]->notes & (1 << i)) != 0)
							{
								sudoku[x][currentBlock->nColumn]->possible--;
								sudoku[x][currentBlock->nColumn]->notes ^= (1 << i);
								csvPrint("%d,%d,|%d\n",sudoku[x][currentBlock->nColumn]->nRow,sudoku[x][currentBlock->nColumn]->nColumn,sudoku[x][currentBlock->nColumn]->notes);
							}
						}
					}
				}
			}
	}
}

//====================================================================
//	Name: 	nakedSubsets
//	Desc:	Find and remove all naked subsets from 2 to nMaxNumber in 
//			rows, columns, and blocks.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	N/A
//====================================================================
void nakedSubsets(Cell*** sudoku, Block*** blocks)
{
	int x;

	for(x = 2; x < nMaxNumber; x++)
	{
		csvPrint("Removing Naked Subsets of %d\n",x);
		nakedSubSetsRow(x, sudoku);
		nakedSubSetsColumn(x, sudoku);
		nakedSubsetsBlocks(x, sudoku, blocks);
	}
}

//====================================================================
//	Name: 	nakedSubSetsRow
//	Desc:	Find and remove all naked subsets from 2 to nMaxNumber in 
//			rows.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	N/A
//====================================================================
void nakedSubSetsRow(int subset, Cell*** sudoku)
{
	int nRow, nColumn, x, i;
	int nCount;

	Cell *currentCell;
	Cell *tempCell;

	csvPrint("Removing Naked Subsets Row of %d\n",subset);
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			currentCell = sudoku[nRow][nColumn];
			if(currentCell->number != unSolved) continue;
			if(currentCell->possible != subset) continue;
			nCount = 1;
			for(x = 0; x < nMaxNumber; x++)
			{
				tempCell = sudoku[nRow][x];

				if(x == nColumn) continue;
				if(tempCell->number != unSolved) continue;
				if(tempCell->possible != subset) continue;

				if(tempCell->notes == currentCell->notes) nCount++;
			}

			if(nCount == subset)
			{
				for(x = 0; x < nMaxNumber; x++)
				{
					tempCell = sudoku[nRow][x];
					if(tempCell->number != unSolved) continue;
					if(tempCell->notes == currentCell->notes) continue;
					for(i = 0; i < nMaxNumber; i++)
					{
						if((currentCell->notes & (1 << i)) == 0) continue;

						if((tempCell->notes & (1 << i)) != 0)
						{
							tempCell->notes ^= (1 << i);
							tempCell->possible--;
							csvPrint("%d,%d,|%d\n",tempCell->nRow,tempCell->nColumn,tempCell->notes);
						}
					}
				}
			}
		}
	}
}

//====================================================================
//	Name: 	nakedSubSetsColumn
//	Desc:	Find and remove all naked subsets from 2 to nMaxNumber in 
//			column.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	N/A
//====================================================================
void nakedSubSetsColumn(int subset, Cell*** sudoku)
{
	int nRow, nColumn, x, i;
	int nCount;

	Cell *currentCell;
	Cell *tempCell;

	csvPrint("Removing Naked Subsets Column of %d\n",subset);
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			currentCell = sudoku[nRow][nColumn];
			if(currentCell->number != unSolved) continue;
			if(currentCell->possible != subset) continue;
			nCount = 1;
			for(x = 0; x < nMaxNumber; x++)
			{
				tempCell = sudoku[x][nColumn];

				if(x == nRow) continue;
				if(tempCell->number != unSolved) continue;
				if(tempCell->possible != subset) continue;

				if(tempCell->notes == currentCell->notes) nCount++;
			}

			if(nCount == subset)
			{
				for(x = 0; x < nMaxNumber; x++)
				{
					tempCell = sudoku[x][nColumn];
					if(tempCell->number != unSolved) continue;
					if(tempCell->notes == currentCell->notes) continue;
					for(i = 0; i < nMaxNumber; i++)
					{
						if((currentCell->notes & (1 << i)) == 0) continue;

						if((tempCell->notes & (1 << i)) != 0)
						{
							tempCell->notes ^= (1 << i);
							tempCell->possible--;
							csvPrint("%d,%d,|%d\n",tempCell->nRow,tempCell->nColumn,tempCell->notes);
						}
					}
				}
			}
		}
	}
}

//====================================================================
//	Name: 	nakedSubsetsBlocks
//	Desc:	Find and remove all naked subsets from 2 to nMaxNumber in 
//			blocks.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	N/A
//====================================================================
void nakedSubsetsBlocks(int subset, Cell*** sudoku, Block*** blocks)
{
	int nRow, nColumn, x, i;
	int nCount;

	Cell *currentCell;
	Cell *tempCell;

	Block *currentBlock;
	Block *tempBlock;
	csvPrint("Removing Naked Subsets Block of %d\n",subset);
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			currentBlock = blocks[nRow][nColumn];
			currentCell = sudoku[currentBlock->nRow][currentBlock->nColumn];
			if(currentCell->number != unSolved) continue;
			if(currentCell->possible != subset) continue;
			nCount = 1;
			for(x = 0; x < nMaxNumber; x++)
			{
				tempBlock = blocks[nRow][x];
				tempCell = sudoku[tempBlock->nRow][tempBlock->nColumn];

				if(x == nColumn) continue;
				if(tempCell->number != unSolved) continue;
				if(tempCell->possible != subset) continue;

				if(tempCell->notes == currentCell->notes) nCount++;
			}

			if(nCount == subset)
			{
				for(x = 0; x < nMaxNumber; x++)
				{
					tempBlock = blocks[nRow][x];
					tempCell = sudoku[tempBlock->nRow][tempBlock->nColumn];
					if(tempCell->number != unSolved) continue;
					if(tempCell->notes == currentCell->notes) continue;
					for(i = 0; i < nMaxNumber; i++)
					{
						if((currentCell->notes & (1 << i)) == 0) continue;

						if((tempCell->notes & (1 << i)) != 0)
						{
							tempCell->notes ^= (1 << i);
							tempCell->possible--;
							csvPrint("%d,%d,|%d\n",tempCell->nRow,tempCell->nColumn,tempCell->notes);
						}
					}
				}
			}
		}
	}
}

//====================================================================
//	Name: 	trialAndErrorSolve
//	Desc:	Solve the soduko puzzle using recursion and brute force.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//			[I/O]	blocks:	A pointer to a two dimensional array of Block.
//	Return:	
//			True: The solution is valid so far.
//			False: The solution is invalid.
//====================================================================
bool trialAndErrorSolve(Cell*** sudoku, Block*** blocks)
{
	int i;

	Cell* currentCell;
	currentCell = findNextEmptyCell(sudoku);

	if(currentCell == NULL) return true;
	for(i = 0; i < nMaxNumber; i++)
	{
		if((currentCell->notes & (1 << i)) != 0)
		{
			currentCell->number = i+1;
			if(isValidSudoku(sudoku,blocks))
			{
				if (trialAndErrorSolve(sudoku, blocks))
				{
					return true;
				}
			}
			currentCell->number = unSolved;
		}
	}
	return false;
}

//====================================================================
//	Name: 	findNextEmptyCell
//	Desc:	Finds the next empty cell and returns it.
//	Args:
//			[I/O]	sudoku:	pointer multidimensional array of the
//							original puzzle.
//	Return:	
//			Cell: The next empty cell.
//			Null: There are no more empty cells.
//====================================================================
Cell* findNextEmptyCell(Cell*** sudoku)
{

	int nRow, nColumn;

	Cell* currentCell;
	for(nRow = 0; nRow < nMaxNumber; nRow++)
	{
		for(nColumn = 0; nColumn < nMaxNumber; nColumn++)
		{
			currentCell = sudoku[nRow][nColumn];
			if(currentCell->number == unSolved)
			{
				return currentCell;
			}
		}
	}
	return NULL;
}

/*
int* allPossibleCombinations(int nNotes, int nPossible)
{
	int numberOfCombinations;
	int *possibleCombinations;
	int loopCombination;
	int currentCombinations;
	int x, i, j;

	numberOfCombinations = 1;
	for (x = 0; x < nPossible; x++)
	{
		numberOfCombinations *=2;
	}
	numberOfCombinations--;

	possibleCombinations = malloc(sizeof(int)*numberOfCombinations);


	currentCombinations = 0;
	for(i = 0; i < nMaxNumber; i++)
	{
		if ((nNotes & (1 << i)) == 0) continue;
		loopCombination = currentCombinations;
		for(j = 0; j < loopCombination; j++)
		{
			possibleCombinations[currentCombinations] = (1 << i) | possibleCombinations[j];
			currentCombinations++;
		}
		possibleCombinations[currentCombinations] = (1 << i);
		currentCombinations++;
	}

	return possibleCombinations;
}
*/
